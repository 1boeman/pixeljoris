<?php
require('./require_login.php');

if (!empty($_FILES['upload']) && $_FILES['upload']['error'] == UPLOAD_ERR_OK) {
    // Be sure we're dealing with an upload
    if (is_uploaded_file($_FILES['upload']['tmp_name']) === false) {
        throw new \Exception('Error on upload: Invalid file definition');
    }
    // Rename the uploaded file
    $uploadName = $_FILES['upload']['name'];
    $ext = strtolower(substr($uploadName, strripos($uploadName, '.')+1));
    $filename = round(microtime(true)).mt_rand().'.'.$ext;

    move_uploaded_file($_FILES['upload']['tmp_name'], __DIR__.'/uploads/'.$filename);
?>
  <script>
  window.location = '<?php echo dirname($_SERVER['REQUEST_URI']).'/uploads/'.$filename; ?>';
  </script>
<?php
}
else { ?>

    <form class="" action="upload.php" method="post" enctype="multipart/form-data">
        Select image to upload:
        <input type="file" name="upload">
        <input type="submit" value="Upload Image" name="submit">       
      </form>

<?php }
